## Sequential
```console
clang++ -O2 -std=c++17 sequential.cpp -DSIZE=64 -DTYPE=uint32_t -o seq
```

## Random
```console
clang++ -O2 -std=c++17 random.cpp -DSIZE=64 -DTYPE=uint32_t -o rng
```

## AES
```console
clang++ -O2 -std=c++17 aes.cpp AES.cpp -DSIZE=128 -DKEY_SIZE=256 -o aes
```
## DCT
```console
clang++ -O2 -std=c++20 dct.cpp -DSIZE=3 -DTYPE=float -o dct
```

## Sources
 - AES: https://github.com/SergeyBel/AES
 - DCT: https://www.geeksforgeeks.org/discrete-cosine-transform-algorithm-program/
