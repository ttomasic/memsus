#include "AES.hpp"

#include <vector>
#include <numeric>

using namespace std;

#ifndef SIZE
#define SIZE 64
#endif

#ifndef KEY_SIZE
#define KEY_SIZE 128
#endif

#define PPCAT_NX(A, B) A ## B
#define PPCAT(A, B) PPCAT_NX(A, B)

#define KEY_TYPE PPCAT(AESKeyLength::AES_, KEY_SIZE)

int main() {
	vector<unsigned char> plain_text(SIZE, 0);
	iota(plain_text.begin(), plain_text.end(), 1);

	vector<unsigned char> key(KEY_SIZE / 8, 0);
	iota(key.begin(), key.end(), 1);

	AES aes(KEY_TYPE);
	auto c = aes.EncryptECB(plain_text, key);

	for(int i = 0; i < 2000; ++i) {
		auto c = aes.EncryptECB(plain_text, key);
	}

	for(auto&& el : c)
		cout << std::hex << el << ' ';
	cout << '\n';

	return 0;
}
