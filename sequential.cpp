#include <cstdint>
#include <iostream>

#ifndef TYPE
#define TYPE uint8_t
#endif

#ifndef SIZE
#define SIZE 64
#endif

int main() {
	TYPE arr[SIZE][SIZE] = {0};
	volatile uint64_t sum = 0;

	for(int cnt = 0; cnt < 2000; ++cnt) {
		for(int i = 0; i < SIZE; ++i) {
			for(int j = 0; j < SIZE; ++j) {
				arr[i][j] = 1;
				sum += arr[i][j];
			}
		}
	}

			
	std::cout << sum << '\n';

	return 0;
}
