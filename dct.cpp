#include <iostream>
#include <cmath>
#include <numbers>
#include <array>

using namespace std;

#ifndef TYPE
#define TYPE float
#endif

#ifndef SIZE
#define SIZE 64
#endif

using matrix_t = array<array<TYPE, SIZE>, SIZE>;
#define pi 3.142857

// Function to find discrete cosine transform and print it
void dctTransform(matrix_t& matrix)
{
    // dct will store the discrete cosine transform
    matrix_t dct;
 
    TYPE ci, cj, dct1, sum;
 
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
 
            // ci and cj depends on frequency as well as
            // number of row and columns of specified matrix
            if (i == 0)
                ci = 1 / sqrt(SIZE);
            else
                ci = sqrt(2) / sqrt(SIZE);
            if (j == 0)
                cj = 1 / sqrt(SIZE);
            else
                cj = sqrt(2) / sqrt(SIZE);
 
            // sum will temporarily store the sum of
            // cosine signals
            sum = 0;
            for (int k = 0; k < SIZE; k++) {
                for (int l = 0; l < SIZE; l++) {
                    dct1 = matrix[k][l] *
                           cos((2 * k + 1) * i * pi / (2 * SIZE)) *
                           cos((2 * l + 1) * j * pi / (2 * SIZE));
                    sum = sum + dct1;
                }
            }
            dct[i][j] = ci * cj * sum;
        }
    }

	for(auto&& row : dct) {
		for(auto&& el : row) cout << el << ' ';
		cout << '\n';
	}
}
 
// Driver code
int main()
{
    matrix_t matrix;

	for(auto& row : matrix)
		for(auto& el : row)
			el = 255.0;

    dctTransform(matrix);

    return 0;
}
