#include <cstdint>
#include <iostream>
#include <random>

using namespace std;

#ifndef TYPE
#define TYPE uint8_t
#endif

#ifndef SIZE
#define SIZE 64
#endif

int main() {
	TYPE arr[SIZE][SIZE] = {0};
	volatile uint64_t sum = 0;

	random_device rd;
	default_random_engine dre(rd());
	uniform_int_distribution<int> rng(0, SIZE - 1);

	for(int c = 0; c < 2000; ++c) {
		for(int cnt = 0; cnt < SIZE*SIZE; ++cnt) {
			auto i = rng(dre);
			auto j = rng(dre);

			arr[i][j] = 1;
			sum += arr[i][j];
		}
	}
	
			
	cout << sum << '\n';

	return 0;
}
